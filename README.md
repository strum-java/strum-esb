[![pipeline](https://gitlab.com/strum-java/strum-esb/badges/main/pipeline.svg)](https://gitlab.com/strum-java/strum-esb/pipelines/latest)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=strum-java_strum-esb&metric=alert_status)](https://sonarcloud.io/dashboard?id=strum-java_strum-esb)
[![Maintainability Rating](https://sonarcloud.io/api/project_badges/measure?project=strum-java_strum-esb&metric=sqale_rating)](https://sonarcloud.io/dashboard?id=strum-java_strum-esb)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=strum-java_strum-esb&metric=coverage)](https://sonarcloud.io/dashboard?id=strum-java_strum-esb)
# strum-esb


Useful sender RabbitMQ messages and handle them
It's implementation of existing open source ruby lib [strum-rb/strum-esb](https://gitlab.com/strum-rb/strum-esb)

## Installation

TBD (need to be added to maven central)

## Usage

### Configuration

Exchanges can be customized by each message type:
```java
EsbConfig config = EsbConfig.builder()
                    .infoExchange("demo.info")
                    .eventExchange("demo.events")
                    .actionExchange("demo.actions")
                    .noticeExchange("demo.notice")
                    .build();
EsbManager manager = new EsbManager(config);
```

Pre publish hooks allow you to configure custom code that will be able to modify properties of the message that will be published or execute any arbitrary logic
TBD

### Sending message:

```java
manager.action(payload,action, resource);
//or
new Action(payload, action, resource).sent(manager.getSender());
```

```java
manager.success(payload, event, resource);
manager.failure(payload, event, resource);
//or
new Event(EventType.SUCCESS, payload,  event, resource).sent(manager.getSender());
new Event(EventType.FAILURE, payload,  event, resource).sent(manager.getSender());
```

```java
manager.info(payload, resource);
// or
new Info(payload, resource).sent(manager.getSender());
```

```java
manager.notice(payload, notice);
manager.notice(payload, notice, resource);
//or
new Notice(payload, notice).sent(manager.getSender());
new Notice(payload, notice, resource).sent(manager.getSender());
```

### Handling message:

```java
public class Handler extends MessageHandler {

    public Handler() {
        super("testQueue", Binding.builder()
                .actions(List.of("create/user"))
                .events(List.of("user/created/success", "user/created/failure"))
                .infos(List.of("user"))
                .notices(List.of("user/failed", "failed"))
                .build());
    }

    @Override
    public void visit(Event e) {
       //handle
    }

    @Override
    public void visit(Info e) {
        //handle
    }

    @Override
    public void visit(Notice e) {
        //handle
    }

    @Override
    public void visit(Action e) {
        //handle
    }

    @Override
    public void onCancel(String consumerTag) {
        //handle
    }
}

```

