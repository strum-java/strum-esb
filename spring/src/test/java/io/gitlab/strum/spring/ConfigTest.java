package io.gitlab.strum.spring;

import io.gitlab.strum.core.EsbManager;
import io.gitlab.strum.spring.stub.TestApp;
import org.junit.jupiter.api.BeforeAll;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.testcontainers.containers.RabbitMQContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

@SpringBootTest(classes = TestApp.class)
@ContextConfiguration(initializers = ConfigTest.Initializer.class)
@TestPropertySource(locations = "classpath:application.yml")
@Testcontainers
public class ConfigTest {

    @Container
    private static final RabbitMQContainer rabbitMqContainer = new RabbitMQContainer("rabbitmq:3.8-management-alpine").withExposedPorts(5672);

    @Autowired
    private EsbManager esbManager;

    @BeforeAll
    static void beforeAll() {
        Initializer.rabbitMqPort = rabbitMqContainer.getMappedPort(5672);
    }

    static class Initializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {
        static Integer rabbitMqPort;

        @Override
        public void initialize(ConfigurableApplicationContext configurableApplicationContext) {
            TestPropertyValues.of("strum.port=" + rabbitMqPort).applyTo(configurableApplicationContext);
        }
    }
}
