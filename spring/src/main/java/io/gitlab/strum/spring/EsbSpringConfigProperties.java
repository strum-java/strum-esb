package io.gitlab.strum.spring;

import io.gitlab.strum.core.EsbConfig;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;


@Data
@ConfigurationProperties("strum")
public class EsbSpringConfigProperties {
    Exchanges exchanges;
    String host;
    Integer port;


    public EsbConfig getConfig() {
        return EsbConfig.builder()
                .infoExchange(exchanges.info)
                .eventExchange(exchanges.event)
                .actionExchange(exchanges.action)
                .noticeExchange(exchanges.notice)
                .host(host)
                .port(port)
                .build();
    }

    @Data
    public static class Exchanges {
        String info;
        String event;
        String action;
        String notice;
    }
}
