package io.gitlab.strum.spring;

import io.gitlab.strum.core.EsbManager;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

@Configuration
public class EsbSchemaUpdater {


    private final EsbManager manager;

    public EsbSchemaUpdater(EsbManager manager) {
        this.manager = manager;
    }

    @PostConstruct
    public void updateSchema() {
        manager.updateBindings();
    }
}
