package io.gitlab.strum.spring;

import io.gitlab.strum.core.EsbConfig;
import io.gitlab.strum.core.EsbManager;
import io.gitlab.strum.core.MessageHandler;
import io.gitlab.strum.core.MessageSender;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;

@Configuration
public class EsbSpringConfig {

    @Bean
    @ConditionalOnMissingBean(EsbManager.class)
    public EsbManager manger(EsbConfig config, List<MessageHandler> handlers) {
        final EsbManager esbManager = new EsbManager(config);
        handlers.forEach(esbManager::addHandler);
        return esbManager;
    }

    @Bean
    @ConditionalOnMissingBean(EsbConfig.class)
    public EsbConfig config(EsbSpringConfigProperties conf) {
        return conf.getConfig();
    }

    @Bean
    @ConditionalOnMissingBean(MessageSender.class)
    public MessageSender sender(EsbManager manager) {
        return manager.getSender();
    }
}
