package io.gitlab.strum.core.stubs;

import com.fasterxml.jackson.annotation.JsonProperty;

public record TestUser(@JsonProperty("username") String username, @JsonProperty("password") String password) {

}
