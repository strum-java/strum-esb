package io.gitlab.strum.core.stubs;

import io.gitlab.strum.core.MessageHandler;
import io.gitlab.strum.core.message.Action;
import io.gitlab.strum.core.message.Notice;
import io.gitlab.strum.core.binding.Binding;
import io.gitlab.strum.core.message.Event;
import io.gitlab.strum.core.message.Info;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

@Slf4j
public class TestUserHandler extends MessageHandler {

    public TestUserHandler() {
        super("testQueue", Binding.builder()
                .actions(List.of("create/user"))
                .events(List.of("user/created/success", "user/created/failure"))
                .infos(List.of("user"))
                .notices(List.of("user/failed", "failed"))
                .build());
    }

    @Override
    public void visit(Event e) {
        super.visit(e);
    }

    @Override
    public void visit(Info e) {
        super.visit(e);
    }

    @Override
    public void visit(Notice e) {
        super.visit(e);
    }

    @Override
    public void visit(Action e) {
        super.visit(e);
    }

    @Override
    public void onCancel(String consumerTag) {
        log.info("onCancel consumerTag:" + consumerTag);
    }
}
