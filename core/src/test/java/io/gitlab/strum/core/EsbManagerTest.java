package io.gitlab.strum.core;

import com.rabbitmq.client.*;
import io.gitlab.strum.core.stubs.TestUser;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.testcontainers.containers.RabbitMQContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.shaded.com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.time.Duration;
import java.util.HashMap;
import java.util.Objects;

import static org.awaitility.Awaitility.await;

@Testcontainers
@Slf4j
class EsbManagerTest {

    @Container
    private static final RabbitMQContainer RABBIT_MQ_CONTAINER = new RabbitMQContainer("rabbitmq:3.8-management-alpine");

    private static EsbManager testInstance;
    private static Channel channel;
    private static EsbConfig config;
    private static final ObjectMapper mapper = new ObjectMapper();

    @BeforeAll
    static void beforeAll() {
        config = EsbConfig.builder()
                .host(RABBIT_MQ_CONTAINER.getHost())
                .port(RABBIT_MQ_CONTAINER.getAmqpPort())
                .infoExchange("demo.info")
                .eventExchange("demo.events")
                .actionExchange("demo.actions")
                .noticeExchange("demo.notice")
                .build();
        testInstance = new EsbManager(config);
        testInstance.updateBindings();
        channel = createChannel();
        log.info("Rabbitmq Amqp port:" + RABBIT_MQ_CONTAINER.getAmqpPort() +
                "http port:" + RABBIT_MQ_CONTAINER.getHttpPort());
    }

    @AfterAll
    static void afterAll() {
        log.info("Rabbitmq Amqp port:" + RABBIT_MQ_CONTAINER.getAmqpPort() +
                "http port:" + RABBIT_MQ_CONTAINER.getHttpPort());
    }

    @Test
    @SneakyThrows
    void shouldSendSuccessEvent() {
        final String testName = "shouldSendSuccessEvent";
        var bindingArgs = getDefaultArgs();
        bindingArgs.put(TestConstants.RESOURCE, TestConstants.RESOURCE_USER);
        bindingArgs.put(TestConstants.EVENT, TestConstants.EVENT_CREATED);
        bindingArgs.put(TestConstants.STATE, TestConstants.STATE_SUCCESS);
        setupTestQueue(testName, config.getEventExchange(), bindingArgs);

        final TestUser testUser = new TestUser("successEventUser", "successEventPass");
        testInstance.success(mapper.writeValueAsBytes(testUser), TestConstants.EVENT_CREATED, TestConstants.RESOURCE_USER);

        final GetResponse response = getResponse(testName);

        Assertions.assertEquals(0, response.getMessageCount());
        final TestUser resultUser = mapper.readValue(response.getBody(), TestUser.class);
        Assertions.assertEquals(testUser, resultUser);
    }

    @Test
    @SneakyThrows
    void shouldSendFailureEvent() {
        final String testName = "shouldSendFailureEvent";
        var bindingArgs = getDefaultArgs();
        bindingArgs.put(TestConstants.RESOURCE, TestConstants.RESOURCE_USER);
        bindingArgs.put(TestConstants.EVENT, TestConstants.EVENT_CREATED);
        bindingArgs.put(TestConstants.STATE, TestConstants.STATE_FAILURE);
        setupTestQueue(testName, config.getEventExchange(), bindingArgs);

        final TestUser testUser = new TestUser("failureEventUser", "failureEventPass");
        testInstance.failure(mapper.writeValueAsBytes(testUser), TestConstants.EVENT_CREATED, TestConstants.RESOURCE_USER);

        final GetResponse response = getResponse(testName);
        Assertions.assertEquals(0, response.getMessageCount());
        final TestUser resultUser = mapper.readValue(response.getBody(), TestUser.class);
        Assertions.assertEquals(testUser, resultUser);
    }


    @Test
    @SneakyThrows
    void shouldSendAction() {
        final String testName = "shouldSendAction";
        var bindingArgs = getDefaultArgs();
        bindingArgs.put(TestConstants.RESOURCE, TestConstants.RESOURCE_USER);
        bindingArgs.put(TestConstants.ACTION, TestConstants.ACTION_CREATE);
        setupTestQueue(testName, config.getActionExchange(), bindingArgs);

        final TestUser testUser = new TestUser("test", "testpass");
        testInstance.action(mapper.writeValueAsBytes(testUser), TestConstants.ACTION_CREATE, TestConstants.RESOURCE_USER);

        final GetResponse response = getResponse(testName);
        Assertions.assertEquals(0, response.getMessageCount());
        final TestUser resultUser = mapper.readValue(response.getBody(), TestUser.class);
        Assertions.assertEquals(testUser, resultUser);
    }

    @Test
    @SneakyThrows
    void shouldSendInfo() {
        final String testName = "shouldSendInfo";
        var bindingArgs = getDefaultArgs();
        bindingArgs.put(TestConstants.RESOURCE, TestConstants.RESOURCE_USER);
        setupTestQueue(testName, config.getInfoExchange(), bindingArgs);

        final TestUser testUser = new TestUser("test", "testpass");
        testInstance.info(mapper.writeValueAsBytes(testUser), "user");

        final GetResponse response = getResponse(testName);
        Assertions.assertEquals(0, response.getMessageCount());
        final TestUser resultUser = mapper.readValue(response.getBody(), TestUser.class);
        Assertions.assertEquals(testUser, resultUser);
    }

    @Test
    @SneakyThrows
    void shouldSendNotice() {
        final String testName = "shouldSendNotice";
        var bindingArgs = getDefaultArgs();
        bindingArgs.put(TestConstants.RESOURCE, TestConstants.RESOURCE_USER);
        bindingArgs.put(TestConstants.NOTICE, TestConstants.NOTICE_FAILED);
        setupTestQueue(testName, config.getNoticeExchange(), bindingArgs);

        final TestUser testUser = new TestUser("test", "testpass");
        testInstance.notice(mapper.writeValueAsBytes(testUser), TestConstants.NOTICE_FAILED, TestConstants.RESOURCE_USER);

        final GetResponse response = getResponse(testName);
        Assertions.assertEquals(0, response.getMessageCount());
        final TestUser resultUser = mapper.readValue(response.getBody(), TestUser.class);
        Assertions.assertEquals(testUser, resultUser);
    }

    @Test
    @SneakyThrows
    void shouldSendNoticeWithoutResource() {
        final String testName = "shouldSendNoticeWithoutResource";
        var bindingArgs = getDefaultArgs();
        bindingArgs.put(TestConstants.NOTICE, TestConstants.NOTICE_FAILED);
        setupTestQueue(testName, config.getNoticeExchange(), bindingArgs);

        final TestUser testUser = new TestUser("test", "testpass");
        testInstance.notice(mapper.writeValueAsBytes(testUser), TestConstants.NOTICE_FAILED);

        final GetResponse response = getResponse(testName);
        Assertions.assertEquals(0, response.getMessageCount());
        final TestUser resultUser = mapper.readValue(response.getBody(), TestUser.class);
        Assertions.assertEquals(testUser, resultUser);
    }

    private GetResponse getResponse(String testName) {
        return await().atLeast(Duration.ofMillis(100))
                .atMost(Duration.ofSeconds(1))
                .with()
                .pollInterval(Duration.ofMillis(100))
                .until(() -> channel.basicGet(testName, true), Objects::nonNull);
    }

    @SneakyThrows
    private static Channel createChannel() {
        var factory = new ConnectionFactory();
        factory.setHost(RABBIT_MQ_CONTAINER.getHost());
        factory.setPort(RABBIT_MQ_CONTAINER.getAmqpPort());
        return factory.newConnection().createChannel();
    }


    private HashMap<String, Object> getDefaultArgs() {
        var bindingArgs = new HashMap<String, Object>();
        bindingArgs.put("x-match", "all");
        return bindingArgs;
    }

    private void setupTestQueue(String testName, String exchange, HashMap<String, Object> bindingArgs) throws IOException {
        channel.queueDeclare(testName, true, false, false, null);
        channel.queueBind(testName, exchange, "", bindingArgs);
    }
}