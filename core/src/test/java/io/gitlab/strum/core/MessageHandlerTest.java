package io.gitlab.strum.core;

import com.rabbitmq.client.Delivery;
import io.gitlab.strum.core.message.*;
import io.gitlab.strum.core.stubs.TestUser;
import io.gitlab.strum.core.stubs.TestUserHandler;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.junit.jupiter.MockitoExtension;
import org.testcontainers.containers.RabbitMQContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.shaded.com.fasterxml.jackson.databind.ObjectMapper;

import static org.mockito.Mockito.*;

@Testcontainers
@Slf4j
@ExtendWith(MockitoExtension.class)
class MessageHandlerTest {

    @Container
    private static final RabbitMQContainer RABBIT_MQ_CONTAINER = new RabbitMQContainer("rabbitmq:3.8-management-alpine");
    public static final int TIMEOUT = 100;

    private static EsbConfig config;
    private static EsbManager esbManager;
    private static final ObjectMapper mapper = new ObjectMapper();
    static TestUserHandler testHandler;

    @BeforeAll
    static void beforeAll() {
        config = EsbConfig.builder()
                .host(RABBIT_MQ_CONTAINER.getHost())
                .port(RABBIT_MQ_CONTAINER.getAmqpPort())
                .infoExchange("demo.info")
                .eventExchange("demo.events")
                .actionExchange("demo.actions")
                .noticeExchange("demo.notice")
                .build();

        testHandler = spy(new TestUserHandler());
        esbManager = new EsbManager(config);
        esbManager.addHandler(testHandler);
        esbManager.updateBindings();
        log.info("Rabbitmq Amqp port:" + RABBIT_MQ_CONTAINER.getAmqpPort() +
                "http port:" + RABBIT_MQ_CONTAINER.getHttpPort());
    }

    @Test
    @SneakyThrows
    void shouldReceive() {
        final TestUser testUser = new TestUser("successEventUser", "successEventPass");
        final byte[] payload = mapper.writeValueAsBytes(testUser);
        esbManager.success(payload, TestConstants.EVENT_CREATED, TestConstants.RESOURCE_USER);

        ArgumentCaptor<Delivery> captor = ArgumentCaptor.forClass(Delivery.class);
        verify(testHandler, atLeastOnce()).onReceive(anyString(), captor.capture());
        final Delivery value = captor.getValue();
        Assertions.assertArrayEquals(value.getBody(), payload);
    }

    @Test
    @SneakyThrows
    void shouldReceiveSuccessEvent() {
        final TestUser testUser = new TestUser("successEventUser", "successEventPass");
        final byte[] payload = mapper.writeValueAsBytes(testUser);
        esbManager.success(payload, TestConstants.EVENT_CREATED, TestConstants.RESOURCE_USER);
        sleep();

        ArgumentCaptor<Event> captor = ArgumentCaptor.forClass(Event.class);
        verify(testHandler, atLeastOnce()).visit(captor.capture());
        final Event value = captor.getValue();
        Assertions.assertArrayEquals(value.getPayload(), payload);
        Assertions.assertEquals(TestConstants.EVENT_CREATED, value.getValue());
        Assertions.assertEquals(TestConstants.RESOURCE_USER, value.getResource());
        Assertions.assertEquals(EventType.SUCCESS, value.getType());
    }

    @SneakyThrows
    @SuppressWarnings("java:S2925") // sleep is not bad pattern here we need delay to make sure event is processed
    private void sleep() {
        Thread.sleep(TIMEOUT);
    }

    @Test
    @SneakyThrows
    void shouldReceiveFailureEvent() {
        final TestUser testUser = new TestUser("successEventUser", "successEventPass");
        final byte[] payload = mapper.writeValueAsBytes(testUser);
        esbManager.failure(payload, TestConstants.EVENT_CREATED, TestConstants.RESOURCE_USER);
        sleep();

        ArgumentCaptor<Event> captor = ArgumentCaptor.forClass(Event.class);
        verify(testHandler, atLeastOnce()).visit(captor.capture());
        final Event value = captor.getValue();
        Assertions.assertArrayEquals(value.getPayload(), payload);
        Assertions.assertEquals(TestConstants.EVENT_CREATED, value.getValue());
        Assertions.assertEquals(TestConstants.RESOURCE_USER, value.getResource());
        Assertions.assertEquals(EventType.FAILURE, value.getType());
    }

    @Test
    @SneakyThrows
    void shouldReceiveInfo() {
        final TestUser testUser = new TestUser("successEventUser", "successEventPass");
        final byte[] payload = mapper.writeValueAsBytes(testUser);
        esbManager.info(payload, TestConstants.RESOURCE_USER);
        sleep();

        ArgumentCaptor<Info> captor = ArgumentCaptor.forClass(Info.class);
        verify(testHandler, atLeastOnce()).visit(captor.capture());
        final Info value = captor.getValue();
        Assertions.assertArrayEquals(value.getPayload(), payload);
        Assertions.assertEquals(TestConstants.RESOURCE_USER, value.getResource());
    }

    @Test
    @SneakyThrows
    void shouldReceiveAction() {
        final TestUser testUser = new TestUser("successEventUser", "successEventPass");
        final byte[] payload = mapper.writeValueAsBytes(testUser);
        esbManager.action(payload, TestConstants.ACTION_CREATE, TestConstants.RESOURCE_USER);
        sleep();

        ArgumentCaptor<Action> captor = ArgumentCaptor.forClass(Action.class);
        verify(testHandler, atLeastOnce()).visit(captor.capture());
        final Action value = captor.getValue();
        Assertions.assertArrayEquals(value.getPayload(), payload);
        Assertions.assertEquals(TestConstants.RESOURCE_USER, value.getResource());
        Assertions.assertEquals(TestConstants.ACTION_CREATE, value.getValue());
    }

    @Test
    @SneakyThrows
    void shouldReceiveNotice() {
        final TestUser testUser = new TestUser("successEventUser", "successEventPass");
        final byte[] payload = mapper.writeValueAsBytes(testUser);
        esbManager.notice(payload, TestConstants.NOTICE_FAILED, TestConstants.RESOURCE_USER);
        sleep();

        ArgumentCaptor<Notice> captor = ArgumentCaptor.forClass(Notice.class);
        verify(testHandler, atLeastOnce()).visit(captor.capture());
        final Notice value = captor.getValue();
        Assertions.assertArrayEquals(value.getPayload(), payload);
        Assertions.assertEquals(TestConstants.RESOURCE_USER, value.getResource());
        Assertions.assertEquals(TestConstants.NOTICE_FAILED, value.getValue());
    }

    @Test
    @SneakyThrows
    void shouldReceiveNoticeWithoutResource() {
        final TestUser testUser = new TestUser("successEventUser", "successEventPass");
        final byte[] payload = mapper.writeValueAsBytes(testUser);
        esbManager.notice(payload, TestConstants.NOTICE_FAILED);
        sleep();

        ArgumentCaptor<Notice> captor = ArgumentCaptor.forClass(Notice.class);
        verify(testHandler, atLeastOnce()).visit(captor.capture());
        final Notice value = captor.getValue();
        Assertions.assertArrayEquals(value.getPayload(), payload);
        Assertions.assertEquals(TestConstants.NOTICE_FAILED, value.getValue());
    }
}
