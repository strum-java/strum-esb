package io.gitlab.strum.core;

import lombok.experimental.UtilityClass;

@UtilityClass
public class TestConstants {
    public static final String EVENT_CREATED = "created";
    public static final String STATE_SUCCESS = "success";
    public static final String STATE_FAILURE = "failure";
    public static final String STATE = "state";
    public static final String RESOURCE = "resource";
    public static final String EVENT = "event";
    public static final String ACTION = "action";
    public static final String ACTION_CREATE = "create";
    public static final String NOTICE = "notice";
    public static final String NOTICE_FAILED = "failed";
    public static final String RESOURCE_USER = "user";
}
