package io.gitlab.strum.core;

import com.rabbitmq.client.CancelCallback;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DeliverCallback;
import com.rabbitmq.client.Delivery;
import io.gitlab.strum.core.binding.Binding;
import io.gitlab.strum.core.message.*;
import lombok.Value;
import lombok.experimental.NonFinal;
import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.util.Map;

import static io.gitlab.strum.core.util.Constants.MessageType.*;
import static io.gitlab.strum.core.util.Constants.*;

@Value
@NonFinal
@Slf4j
public abstract class MessageHandler implements DeliverCallback, CancelCallback, MessageVisitor {

    public static final String GOT_UNHANDLED_MESSAGE = "Got unhandled message {}";
    String fromQueue;
    Binding binding;

    @NonFinal
    Channel channel;

    protected MessageHandler(String fromQueue, Binding binding) {
        this.fromQueue = fromQueue;
        this.binding = binding;
    }

    public abstract void onCancel(String consumerTag);

    public void onReceive(String consumerTag, Delivery message) {
        log.debug("Got message {} with tag {}", message, consumerTag);
    }

    public void onRegistration(Channel channel) {
        this.channel = channel;
    }

    @Override
    public void handle(String consumerTag) {
        try {
            this.onCancel(consumerTag);
        } catch (Exception e) {
            log.error("Error on cancel", e);
        }
    }

    @Override
    public void handle(String consumerTag, Delivery message) throws IOException {
        try {
            final Map<String, Object> headers = message.getProperties().getHeaders();

            this.onReceive(consumerTag, message);
            if (headers.containsKey(ACTION)) {
                visit(new Action(message.getBody(), getAction(headers), getResource(headers)));
            } else if (headers.containsKey(EVENT)) {
                visit(new Event(getEventType(headers), message.getBody(), getEvent(headers), getResource(headers)));
            } else if (headers.containsKey(NOTICE)) {
                visit(new Notice(message.getBody(), getNotice(headers), getResource(headers)));
            } else if (headers.containsKey(INFO)) {
                visit(new Info(message.getBody(), getResource(headers)));
            }
            getChannel().basicAck(message.getEnvelope().getDeliveryTag(), false);
        } catch (Exception e) {
            getChannel().basicReject(message.getEnvelope().getDeliveryTag(), false);
            log.error("Error on receive", e);
        }
    }

    @Override
    public void visit(Event e) {
        log.info(GOT_UNHANDLED_MESSAGE, e);
    }

    @Override
    public void visit(Info e) {
        log.info(GOT_UNHANDLED_MESSAGE, e);
    }

    @Override
    public void visit(Notice e) {
        log.info(GOT_UNHANDLED_MESSAGE, e);
    }

    @Override
    public void visit(Action e) {
        log.info(GOT_UNHANDLED_MESSAGE, e);
    }

    private String getResource(Map<String, Object> headers) {
        final Object resource = headers.get(RESOURCE);
        return resource != null ? resource.toString() : null;
    }

    private String getAction(Map<String, Object> headers) {
        final Object action = headers.get(ACTION);
        return action != null ? action.toString() : null;
    }

    private EventType getEventType(Map<String, Object> headers) {
        return EventType.getByValue(headers.get(STATE).toString());
    }

    private String getEvent(Map<String, Object> headers) {
        final Object event = headers.get(EVENT);
        return event != null ? event.toString() : null;
    }

    private String getNotice(Map<String, Object> headers) {
        final Object event = headers.get(NOTICE);
        return event != null ? event.toString() : null;
    }
}
