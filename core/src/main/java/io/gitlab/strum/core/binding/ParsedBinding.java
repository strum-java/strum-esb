package io.gitlab.strum.core.binding;

import java.util.Map;

public record ParsedBinding (String exchange, Map<String,String> headers){
}
