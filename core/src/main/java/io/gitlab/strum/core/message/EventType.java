package io.gitlab.strum.core.message;

import io.gitlab.strum.core.util.Constants;

import java.util.Arrays;

public enum EventType {
    SUCCESS(Constants.SUCCESS), FAILURE(Constants.FAILURE);

    private final String value;

    EventType(String value) {

        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static EventType getByValue(String value) {
        return Arrays.stream(EventType.values())
                .filter(eventType -> eventType.value.equals(value))
                .findFirst()
                .orElse(null);
    }
}
