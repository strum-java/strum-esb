package io.gitlab.strum.core.message;

import io.gitlab.strum.core.MessageVisitor;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.experimental.FieldDefaults;

import java.util.HashMap;
import java.util.Map;


@Getter
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public abstract class BaseMessage {

    byte[] payload;
    String resource;
    Map<String, Object> headers = new HashMap<>();


    protected BaseMessage(byte[] payload, String resource) {
        this.payload = payload;
        this.resource = resource;
    }

    protected BaseMessage(byte[] payload) {
        this.payload = payload;
        this.resource = null;
    }

    public abstract void sent(MessageVisitor s);

}
