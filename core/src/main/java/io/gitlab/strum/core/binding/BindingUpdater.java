package io.gitlab.strum.core.binding;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import io.gitlab.strum.core.EsbConfig;
import io.gitlab.strum.core.MessageHandler;
import io.gitlab.strum.core.pool.ChannelPool;
import io.gitlab.strum.core.util.ThrowingConsumer;
import lombok.SneakyThrows;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class BindingUpdater {

    private final ChannelPool pool;
    private final BindingParser parser;

    public BindingUpdater(ChannelPool pool, EsbConfig config) {
        this.pool = pool;
        parser = new BindingParser(config);
    }

    public void update(MessageHandler messageHandler) {
        pool.withChannel(channel -> processUpdate(messageHandler.getFromQueue(), messageHandler.getBinding(), channel));

    }

    @SneakyThrows
    private void processUpdate(String fromQueue, Binding binding, Channel channel) {
        final Map<String, List<String>> bindingMap = binding.getBindingMap();
        bindingMap.forEach((type, values) -> {
            final var headerBindings = values.stream().map(s1 -> parser.parse(type, s1)).collect(Collectors.toList());

            headerBindings.forEach(ThrowingConsumer.unchecked(parsedBinding -> {
                var bindingArgs = new HashMap<String, Object>(parsedBinding.headers());
                bindingArgs.put("x-match", "all");
                channel.queueDeclare(fromQueue, true, false, false, null);
                channel.exchangeDeclare(parsedBinding.exchange(), BuiltinExchangeType.HEADERS, true);
                channel.queueBind(fromQueue, parsedBinding.exchange(), "", bindingArgs);
            }));
        });
    }
}
