package io.gitlab.strum.core.message;

import io.gitlab.strum.core.util.Constants;
import io.gitlab.strum.core.MessageVisitor;
import lombok.Getter;
import lombok.ToString;

@ToString
public class Event extends BaseMessage {

    @Getter
    private final EventType type;

    @Getter
    private final String value;

    public Event(EventType type, byte[] payload, String value, String resource) {
        super(payload, resource);
        this.type = type;
        this.value = value;
        var headers = getHeaders();
        headers.put(Constants.RESOURCE, resource);
        headers.put(Constants.MessageType.EVENT, value);
        headers.put(Constants.STATE, type.getValue());
    }

    @Override
    public void sent(MessageVisitor s) {
        s.visit(this);
    }
}
