package io.gitlab.strum.core.util;

import java.util.function.Consumer;

public interface ThrowingConsumer<T, E extends Throwable> {

    void accept(T t) throws E;

    @SuppressWarnings({"java:S1181","java:S112"})
    static <T> Consumer<T> unchecked(final ThrowingConsumer<? super T, ?> function) {
        return t -> {
            try {
                function.accept(t);
            } catch (final Throwable e) {
                throw new RuntimeException(e);
            }
        };
    }
}
