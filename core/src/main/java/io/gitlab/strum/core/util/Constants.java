package io.gitlab.strum.core.util;

import lombok.experimental.UtilityClass;

@UtilityClass
public class Constants {
    @UtilityClass
    public static class MessageType {
        public final String INFO = "info";
        public final String ACTION = "action";
        public final String NOTICE = "notice";
        public final String EVENT = "event";
    }

    public final String RESOURCE = "resource";
    public final String STATE = "state";
    public final String SUCCESS = "success";
    public final String FAILURE = "failure";
}
