package io.gitlab.strum.core.message;

import io.gitlab.strum.core.MessageVisitor;
import io.gitlab.strum.core.util.Constants;
import lombok.ToString;

@ToString
public class Info extends BaseMessage {

    public Info(byte[] payload, String resource) {
        super(payload, resource);
        var headers = getHeaders();
        headers.put(Constants.RESOURCE, resource);
        headers.put(Constants.MessageType.INFO, resource);
    }

    @Override
    public void sent(MessageVisitor s) {
        s.visit(this);
    }

}
