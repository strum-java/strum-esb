package io.gitlab.strum.core.binding;

import io.gitlab.strum.core.EsbConfig;

import java.util.Map;
import java.util.function.Function;

import static io.gitlab.strum.core.util.Constants.*;
import static io.gitlab.strum.core.util.Constants.MessageType.*;

public class BindingParser {

    private final EsbConfig config;

    private final Map<String, Function<String, ParsedBinding>> parsers = Map.of(
            INFO, this::parseInfo,
            ACTION, this::parseAction,
            NOTICE, this::parseNotice,
            EVENT, this::parseEvent
    );


    public BindingParser(EsbConfig config) {
        this.config = config;
    }

    public ParsedBinding parse(String type, String value) {
        return parsers.get(type).apply(value);
    }


    /**
     * exchange:action/resource
     */
    private ParsedBinding parseAction(String s) {
        final String[] sourceWithExchange = parseExchange(s, ACTION);
        final String[] properties = sourceWithExchange[1].split("/");

        return new ParsedBinding(sourceWithExchange[0], Map.of(
                ACTION, properties[0],
                RESOURCE, properties[1]));
    }

    /**
     * exchange:resource/event{/state}
     */
    private ParsedBinding parseEvent(String s) {
        final String[] sourceWithExchange = parseExchange(s, EVENT);
        final String[] properties = sourceWithExchange[1].split("/");

        return new ParsedBinding(sourceWithExchange[0], Map.of(
                RESOURCE, properties[0],
                EVENT, properties[1],
                STATE, properties.length > 2 ? properties[2] : SUCCESS));
    }

    /**
     * exchange:resource/notice
     */
    private ParsedBinding parseNotice(String s) {
        final String[] sourceWithExchange = parseExchange(s, NOTICE);
        final String[] properties = sourceWithExchange[1].split("/");
        if (properties.length > 1) {
            return new ParsedBinding(sourceWithExchange[0], Map.of(RESOURCE, properties[0], NOTICE, properties[1]));
        } else {
            return new ParsedBinding(sourceWithExchange[0], Map.of(NOTICE, properties[0]));
        }
    }

    /**
     * exchange:resource
     */
    private ParsedBinding parseInfo(String s) {
        final String[] sourceWithExchange = parseExchange(s, INFO);
        return new ParsedBinding(sourceWithExchange[0], Map.of(RESOURCE, sourceWithExchange[1]));
    }

    private String[] parseExchange(String source, String type) {
        return source.contains(":") ? source.split(":") : new String[]{config.getExchangeMap().get(type), source};
    }
}
