package io.gitlab.strum.core.pool;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import io.gitlab.strum.core.util.ThrowingConsumer;
import lombok.SneakyThrows;
import org.apache.commons.pool2.impl.GenericObjectPool;

import java.util.function.Consumer;

public class ChannelPool extends GenericObjectPool<Channel> {

    public ChannelPool(Connection connection) {
        super(new ChannelFactory(connection));
        this.setTestOnReturn(true);
        this.setTestOnBorrow(true);
    }

    @SneakyThrows
    public void withChannel(Consumer<Channel> consumer) {
        this.withChannelTrowing(consumer::accept);
    }

    @SneakyThrows
    @SuppressWarnings("java:S112")
    public void withChannelTrowing(ThrowingConsumer<Channel, Exception> consumer) {
        final Channel channel = this.borrowObject();
        try {
            consumer.accept(channel);
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            this.returnObject(channel);
        }
    }

}
