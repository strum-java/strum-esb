package io.gitlab.strum.core;

import io.gitlab.strum.core.message.Action;
import io.gitlab.strum.core.message.Event;
import io.gitlab.strum.core.message.Notice;
import io.gitlab.strum.core.message.Info;

public interface MessageVisitor {

    void visit(Event e);

    void visit(Info e);

    void visit(Notice e);

    void visit(Action e);

}
