package io.gitlab.strum.core;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.BuiltinExchangeType;
import io.gitlab.strum.core.message.*;
import io.gitlab.strum.core.pool.ChannelPool;
import lombok.SneakyThrows;
import org.slf4j.MDC;

import java.util.HashMap;
import java.util.Map;

public class RabbitMessageSender implements MessageSender {

    private final ChannelPool pool;
    private final EsbConfig config;

    public RabbitMessageSender(ChannelPool pool, EsbConfig config) {
        this.pool = pool;
        this.config = config;
    }

    @SneakyThrows()
    public void processSend(BaseMessage message, String exchange) {
        pool.withChannelTrowing(channel -> {
            var properties = new AMQP.BasicProperties.Builder()
                    .headers(modifyHeaders(message))
                    .contentType("application/json")
                    .build();

            channel.exchangeDeclare(exchange, BuiltinExchangeType.HEADERS, true);
            channel.basicPublish(exchange, "", properties, message.getPayload());
        });
    }

    @Override
    public void visit(Event e) {
        processSend(e, config.getEventExchange());
    }

    @Override
    public void visit(Info e) {
        processSend(e, config.getInfoExchange());
    }

    @Override
    public void visit(Notice e) {
        processSend(e, config.getNoticeExchange());
    }

    @Override
    public void visit(Action e) {
        processSend(e, config.getActionExchange());
    }

    private String getPipeline() {
        return MDC.get("pipeline");
    }

    private String getPipelineId() {
        return MDC.get("pipeline-id");
    }


    private Map<String, Object> modifyHeaders(BaseMessage message) {
        var result = new HashMap<>(message.getHeaders());
        result.put("pipeline", getPipeline());
        result.put("pipeline-id", getPipelineId());
        return result;
    }

}
