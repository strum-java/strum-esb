package io.gitlab.strum.core;

import lombok.Builder;
import lombok.Value;
import lombok.experimental.NonFinal;

import java.util.Map;

import static io.gitlab.strum.core.util.Constants.MessageType.*;

@Builder
@Value
public class EsbConfig {

    String infoExchange;
    String eventExchange;
    String actionExchange;
    String noticeExchange;

    String host;
    Integer port;
    @NonFinal
    Map<String, String> exchangeMap;


    public Map<String, String> getExchangeMap() {
        if (exchangeMap == null) {
            exchangeMap = Map.of(
                    INFO, infoExchange,
                    EVENT, eventExchange,
                    ACTION, actionExchange,
                    NOTICE, noticeExchange);
        }
        return exchangeMap;
    }

}
