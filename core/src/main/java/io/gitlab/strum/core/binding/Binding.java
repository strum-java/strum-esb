package io.gitlab.strum.core.binding;


import io.gitlab.strum.core.util.Constants;
import lombok.Builder;
import lombok.Value;
import lombok.experimental.NonFinal;

import java.util.List;
import java.util.Map;

@Value
@Builder
public class Binding {

    List<String> events;
    List<String> actions;
    List<String> notices;
    List<String> infos;

    @NonFinal
    Map<String, List<String>> bindingMap;

    public Map<String, List<String>> getBindingMap() {
        if (bindingMap == null) {
            bindingMap = Map.of(
                    Constants.MessageType.INFO, infos,
                    Constants.MessageType.EVENT, events,
                    Constants.MessageType.ACTION, actions,
                    Constants.MessageType.NOTICE, notices);
        }
        return bindingMap;
    }
}
