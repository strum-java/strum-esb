package io.gitlab.strum.core.message;

import io.gitlab.strum.core.util.Constants;
import io.gitlab.strum.core.MessageVisitor;
import lombok.Getter;
import lombok.ToString;

@ToString
public class Action extends BaseMessage {

    @Getter
    private final String value;

    public Action(byte[] payload, String value, String resource) {
        super(payload, resource);
        this.value = value;
        var headers = getHeaders();
        headers.put(Constants.RESOURCE, resource);
        headers.put(Constants.MessageType.ACTION, value);
    }

    @Override
    public void sent(MessageVisitor s) {
        s.visit(this);
    }

}
