package io.gitlab.strum.core.message;

import io.gitlab.strum.core.util.Constants;
import io.gitlab.strum.core.MessageVisitor;
import lombok.Getter;
import lombok.ToString;

@ToString
public class Notice extends BaseMessage {

    @Getter
    private final String value;

    public Notice(byte[] payload, String value, String resource) {
        super(payload, resource);
        this.value = value;
        var headers = getHeaders();
        headers.put(Constants.MessageType.NOTICE, value);
        headers.put(Constants.RESOURCE, resource);
    }

    public Notice(byte[] payload, String value) {
        super(payload);
        this.value = value;
        var headers = getHeaders();
        headers.put(Constants.MessageType.NOTICE, value);

    }

    @Override
    public void sent(MessageVisitor s) {
        s.visit(this);
    }

}
