package io.gitlab.strum.core;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.ConnectionFactory;
import io.gitlab.strum.core.message.*;
import io.gitlab.strum.core.pool.ChannelPool;
import io.gitlab.strum.core.binding.BindingUpdater;
import lombok.Getter;
import lombok.SneakyThrows;

import java.util.HashSet;
import java.util.Set;

public class EsbManager {

    private final EsbConfig config;
    @Getter
    private final MessageSender sender;
    private final BindingUpdater bindingUpdater;
    private final ChannelPool pool;

    private final Set<MessageHandler> handlers = new HashSet<>();

    @SneakyThrows
    public EsbManager(EsbConfig config) {
        this.config = config;
        var factory = new ConnectionFactory();
        factory.setHost(config.getHost());
        factory.setPort(config.getPort());
        pool = new ChannelPool(factory.newConnection());
        sender = new RabbitMessageSender(pool, config);
        bindingUpdater = new BindingUpdater(pool, config);
    }

    public void action(byte[] payload, String action, String resource) {
        sender.visit(new Action(payload, action, resource));
    }

    public void success(byte[] payload, String event, String resource) {
        sender.visit(new Event(EventType.SUCCESS, payload, event, resource));
    }

    public void failure(byte[] payload, String event, String resource) {
        sender.visit(new Event(EventType.FAILURE, payload, event, resource));
    }

    public void info(byte[] payload, String resource) {
        sender.visit(new Info(payload, resource));
    }

    public void notice(byte[] payload, String notice, String resource) {
        sender.visit(new Notice(payload, notice, resource));
    }

    public void notice(byte[] payload, String notice) {
        sender.visit(new Notice(payload, notice));
    }

    public void addHandler(MessageHandler handler) {
        pool.withChannelTrowing(channel -> {
            handlers.add(handler);
            handler.onRegistration(channel);
            channel.queueDeclare(handler.getFromQueue(), true, false, false, null);
            channel.basicConsume(handler.getFromQueue(), handler, handler);
        });
    }

    public void updateBindings() {
        declareExchanges();
        handlers.forEach(bindingUpdater::update);
    }

    private void declareExchanges() {
        pool.withChannelTrowing(channel -> {
            for (String exchange : config.getExchangeMap().values())
                channel.exchangeDeclare(exchange, BuiltinExchangeType.HEADERS, true);
        });
    }

}
